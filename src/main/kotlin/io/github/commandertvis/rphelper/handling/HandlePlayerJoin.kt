package io.github.commandertvis.rphelper.handling

import io.github.commandertvis.plugin.handle
import io.github.commandertvis.rphelper.datastorage.PlayerResourcePackManager
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.Plugin

fun Plugin.handlePlayerJoin() = handle<PlayerJoinEvent> {
    if (player in PlayerResourcePackManager)
        PlayerResourcePackManager.send(player)
}
