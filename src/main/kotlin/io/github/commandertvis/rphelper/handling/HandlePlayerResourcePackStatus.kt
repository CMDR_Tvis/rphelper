package io.github.commandertvis.rphelper.handling

import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.replyComponents
import io.github.commandertvis.rphelper.PLUGIN_NAME
import io.github.commandertvis.rphelper.messages
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.HoverEvent
import org.bukkit.event.player.PlayerResourcePackStatusEvent
import org.bukkit.plugin.Plugin

fun Plugin.handlePlayerResourcePackStatus() = handle<PlayerResourcePackStatusEvent> {
    when (status) {
        PlayerResourcePackStatusEvent.Status.FAILED_DOWNLOAD, PlayerResourcePackStatusEvent.Status.DECLINED -> {
            player.replyComponents {
                text { +messages.loadFailure }

                text {
                    +messages.clickToTryAgain
                    onHover(HoverEvent.Action.SHOW_TEXT) { text { +messages.tryAgain } }
                    onClick(ClickEvent.Action.RUN_COMMAND, "/$PLUGIN_NAME:rp receive")
                }
            }
        }

        PlayerResourcePackStatusEvent.Status.SUCCESSFULLY_LOADED ->
            player.sendMessage(messages.success)

        PlayerResourcePackStatusEvent.Status.ACCEPTED -> Unit
    }
}
