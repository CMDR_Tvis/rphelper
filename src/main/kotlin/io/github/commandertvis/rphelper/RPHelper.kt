package io.github.commandertvis.rphelper

import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.json.JsonConfigurablePlugin
import io.github.commandertvis.plugin.register
import io.github.commandertvis.rphelper.datastorage.PlayerResourcePackManager
import io.github.commandertvis.rphelper.datastorage.ResourcePack
import io.github.commandertvis.rphelper.datastorage.Settings
import io.github.commandertvis.rphelper.handling.handlePlayerJoin
import io.github.commandertvis.rphelper.handling.handlePlayerResourcePackStatus
import io.github.commandertvis.rphelper.subcommands.*
import org.bukkit.permissions.Permission

const val PLUGIN_NAME = "RPHelper"
lateinit var plugin: RPHelper

var messages
    get() = plugin.jsonConfig.messages
    set(value) = plugin.run { jsonConfig.messages = value; saveConfig() }

var resourcePacks
    get() = plugin.jsonConfig.resourcePacks
    set(value) = plugin.run { jsonConfig.resourcePacks = value; saveConfig() }

class RPHelper : JsonConfigurablePlugin<Settings>(Settings()) {
    override fun onEnable() {
        plugin = this
        registerCommand()
        registerListeners()
        registerPermissions()
    }

    private fun registerCommand() {
        command(setOf("rp", "resourcepacks")) {
            description = messages.command.description
            guiActions()
            info()
            list()
            select()
            reset()
            flush()
            receive()
            reload()
        }
    }

    private fun registerListeners() = run { handlePlayerJoin(); handlePlayerResourcePackStatus() }

    private fun registerPermissions() {
        Permissions.values().forEach(Permission::register)
        resourcePacks.asSequence().map(ResourcePack::permission).forEach(Permission::register)
    }

    override fun reloadConfig() = run { super.reloadConfig(); PlayerResourcePackManager.reloadConfig() }
}
