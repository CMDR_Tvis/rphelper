package io.github.commandertvis.rphelper.datastorage

import org.bukkit.Material
import java.net.URL

class Settings {
    var messages = Messages()

    var resourcePacks = mutableListOf(
        ResourcePack(
            "example",
            URL("https://www.dropbox.com/s/9y1gqa6xfs7l3wk/Faithful-64x64-1.14.4-Stevens-Traditional-64x64-1.14.4.zip?dl=1"),
            "8092d75f9bd385aa24a7ad3c8869b28663b3f52d",
            Material.PAINTING
        )
    )

    class Messages {
        var command = Command()
        var permission = Permission()
        var gui = GUI()
        var tryAgain = "§7Try again..."
        var loadFailure = "§4Failed to load resource pack. "
        var clickToTryAgain = "§4§nClick to §4§ntry again."
        var success = "§2Resource pack was successfully loaded."

        class Command {
            val reloaded = "§7Configuration has been reloaded."
            var clickToGo = "§7Click to go..."
            var clickToCopy = "§7Click to copy..."
            var description = "Allows to manage the server's resource packs."
            var flushed = "§7All the resource packs were flushed!"
            var noPermission = "§4No permission."
            var noSuchResourcePack = "§4Resource pack was not found."
            var nullHash = "none"
            var permissionAvailable = "§2available"
            var permissionNotAvailable = "§4not available"
            var playerOnly = "§4This command is only for players."
            var reset = "§2Your resource pack was reset. You have to rejoin."
            var resetKick = "§2Your resource pack was reset."
            var resourcePack = ResourcePack()
            var resourcePackListHeader = "§7Available resource packs:"
            var resourcePackInList = " — %name% (%available%)."

            var noResourcePackToReset =
                "§4You have no resource pack to reset. You can select one with /rp select."

            var noResourcePackToReceive =
                "§4You have no resource pack to receive. You can select one with /rp select."

            class ResourcePack {
                var header = "§7Resource pack §f%name%:"
                var permissionRubric = "§7 — permission:"
                var permission = "   §f%permission% (%available%§f),"
                var urlRubric = "§7 — URL: "
                var url = "   §f%url%, "
                var hashRubric = "§7 — hash: "
                var hash = "   §f%hash%."
            }
        }

        class GUI {
            var close = "§7Close"
            var current = "§2current"
            var lmbToReset = "§fLMB — reset"
            var nextPage = "§7Next page"
            var rmbToResetAndLeave = "§fRMB — reset and logout"
            var lmbToSelect = "§fLMB — select"
            var packPrefix = "§7"
            var previousPage = "§7Previous page"
            var resetPack = "§4Reset resource pack"
            var returnTo = "§7Return..."
            var returnToGui = "§7§nReturn to GUI."
            var rmbToGetInfo = "§fRMB — information"
            var shiftLmbToFlush = "§f§lShift+RMB — reset for §f§nALL §f§lthe players"
            var title = "§0Resource Packs"
        }

        class Permission {
            var admin = "Allows to execute some privileged commands. Also guarantees access to all the resource packs."
            var user = "Allows to interact with the resourcepack manager."
            var resourcePack = "Allows to install a certain resourcepack."
        }
    }
}
