package io.github.commandertvis.rphelper.datastorage

import io.github.commandertvis.rphelper.PLUGIN_NAME
import io.github.commandertvis.rphelper.messages
import org.bukkit.Material
import org.bukkit.permissions.Permission
import org.bukkit.permissions.PermissionDefault
import java.net.URL
import javax.xml.bind.annotation.adapters.HexBinaryAdapter

data class ResourcePack(
    val name: String,
    val url: URL,
    val hashString: String? = null,
    val guiIcon: Material = Material.PAINTING
) {
    val hash: ByteArray?
        get() = HexBinaryAdapter().unmarshal(hashString)

    val permission
        get() = Permission(
            "$PLUGIN_NAME.resourcepack.$name",
            messages.permission.resourcePack,
            PermissionDefault.TRUE
        )

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true

        if (javaClass != other?.javaClass)
            return false

        other as ResourcePack

        if (name != other.name)
            return false

        return true
    }

    override fun hashCode() = name.hashCode()
}
