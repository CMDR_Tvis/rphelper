package io.github.commandertvis.rphelper.datastorage

import io.github.commandertvis.plugin.json.AdditionalJsonConfiguration
import io.github.commandertvis.plugin.uuidString
import io.github.commandertvis.rphelper.plugin
import io.github.commandertvis.rphelper.resourcePacks
import org.bukkit.OfflinePlayer
import org.bukkit.entity.Player

object PlayerResourcePackManager :
    AdditionalJsonConfiguration<MutableMap<String, String>>(mutableMapOf(), plugin, "players") {

    fun send(player: Player) {
        val resourcePack = this[player] ?: return
        val url = resourcePack.url.toString()
        resourcePack.hash?.let { player.setResourcePack(url, it); return }
        player.setResourcePack(url)
    }

    fun clear() = run { jsonConfig.clear(); saveConfig() }
    operator fun contains(player: OfflinePlayer) = player.uuidString in jsonConfig

    operator fun get(player: OfflinePlayer) = resourcePacks.find { it.name == jsonConfig[player.uuidString] }

    operator fun minusAssign(player: OfflinePlayer) = run<Unit> { jsonConfig.remove(player.uuidString); saveConfig() }

    operator fun set(player: OfflinePlayer, value: ResourcePack) {
        jsonConfig[player.uuidString] = value.name
        saveConfig()
    }
}
