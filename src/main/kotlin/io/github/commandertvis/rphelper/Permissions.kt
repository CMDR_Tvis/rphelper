package io.github.commandertvis.rphelper

import org.bukkit.permissions.Permission
import org.bukkit.permissions.PermissionDefault

object Permissions {
    val ADMIN_PERMISSION =
        Permission(
            "$PLUGIN_NAME.admin",
            messages.permission.admin,
            PermissionDefault.OP,
            mapOf("$PLUGIN_NAME.resourcepack.*" to true)
        )

    val USER_PERMISSION =
        Permission("$PLUGIN_NAME.user", messages.permission.user, PermissionDefault.TRUE)

    fun values() = arrayOf(ADMIN_PERMISSION, USER_PERMISSION)
}
