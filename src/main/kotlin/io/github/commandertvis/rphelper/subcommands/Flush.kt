package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.dsl.ExecutionScope
import io.github.commandertvis.rphelper.Permissions
import io.github.commandertvis.rphelper.datastorage.PlayerResourcePackManager
import io.github.commandertvis.rphelper.messages
import org.bukkit.command.CommandSender

fun ExecutionScope.flush() {
    "flush" subcommand {
        action(@Contexts
        fun CommandSender.(_: String) {
            if (!hasPermission(Permissions.ADMIN_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return
            }

            PlayerResourcePackManager.clear()
            sendMessage(messages.command.flushed)
        })

        tabAction { _, _, _ -> emptyList() }
    }
}
