package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command.contexts.StringContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.dsl.ExecutionScope
import io.github.commandertvis.rphelper.Permissions
import io.github.commandertvis.rphelper.datastorage.PlayerResourcePackManager
import io.github.commandertvis.rphelper.messages
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

fun ExecutionScope.reset() {
    "reset" subcommand {
        action(response = @Contexts(StringContext::class)
        fun CommandSender.(_: String, state: String?) {
            if (!hasPermission(Permissions.USER_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return
            }

            if (this !is Player) {
                sendMessage(messages.command.playerOnly)
                return
            }

            if (this !in PlayerResourcePackManager) {
                sendMessage(messages.command.noResourcePackToReset)
                return
            }

            PlayerResourcePackManager -= this

            if (state == "kick") {
                kickPlayer(messages.command.resetKick)
                return
            }

            sendMessage(messages.command.reset)
        })

        tabAction { _, _, args ->
            if (args.size == 1)
                listOf("kick")
            else
                emptyList()
        }
    }
}
