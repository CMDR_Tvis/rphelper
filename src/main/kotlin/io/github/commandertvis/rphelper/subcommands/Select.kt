package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command.contexts.StringContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.dsl.ExecutionScope
import io.github.commandertvis.rphelper.Permissions
import io.github.commandertvis.rphelper.datastorage.PlayerResourcePackManager
import io.github.commandertvis.rphelper.messages
import io.github.commandertvis.rphelper.resourcePacks
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

fun ExecutionScope.select() {
    "select" subcommand {
        action(response = @Contexts(StringContext::class)
        fun CommandSender.(_: String, name: String?) {
            if (!hasPermission(Permissions.USER_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return
            }

            if (this !is Player) {
                sendMessage(messages.command.playerOnly)
                return
            }

            PlayerResourcePackManager[this] = resourcePacks
                .find { hasPermission(it.permission) && it.name == name }

                ?: run {
                    sendMessage(messages.command.noSuchResourcePack)
                    return
                }

            PlayerResourcePackManager.send(this)
        })

        resourcePackListTabAction()
    }
}
