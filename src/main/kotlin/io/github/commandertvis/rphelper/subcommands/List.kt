package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.dsl.ExecutionScope
import io.github.commandertvis.plugin.placeholders
import io.github.commandertvis.plugin.reply.replyAppending
import io.github.commandertvis.rphelper.Permissions
import io.github.commandertvis.rphelper.messages
import io.github.commandertvis.rphelper.resourcePacks
import org.bukkit.command.CommandSender

fun ExecutionScope.list() {
    "list" subcommand {
        action(@Contexts
        fun CommandSender.(_: String) {
            if (!hasPermission(Permissions.USER_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return
            }

            replyAppending {
                +"${messages.command.resourcePackListHeader}\n"

                resourcePacks.forEachIndexed { i, it ->
                    +messages.command.resourcePackInList.placeholders(
                        "name" to it.name,

                        "available" to if (hasPermission(it.permission))
                            messages.command.permissionAvailable
                        else
                            messages.command.permissionNotAvailable
                    )

                    if (i != resourcePacks.lastIndex)
                        +'\n'
                }
            }
        })

        tabAction { _, _, _ -> emptyList() }
    }
}
