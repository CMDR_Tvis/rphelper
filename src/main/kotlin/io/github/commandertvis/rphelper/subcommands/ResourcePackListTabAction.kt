package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command.dsl.SubcommandScope
import io.github.commandertvis.rphelper.datastorage.ResourcePack
import io.github.commandertvis.rphelper.plugin
import io.github.commandertvis.rphelper.resourcePacks

fun SubcommandScope.resourcePackListTabAction() {
    tabAction { _, _, args ->
        if (args.size == 1)
            return@tabAction resourcePacks
                .asSequence()
                .filter { hasPermission(it.permission) }
                .map(ResourcePack::name)
                .filter { it.startsWith(args[0]) }
                .toList()

        emptyList()
    }
}
