package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.command.contexts.DefaultContext
import io.github.commandertvis.plugin.command.contexts.IntContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.dsl.ExecutionScope
import io.github.commandertvis.plugin.command.parseArguments
import io.github.commandertvis.plugin.gui.dsl.GuiView
import io.github.commandertvis.plugin.gui.showGui
import io.github.commandertvis.plugin.replyComponents
import io.github.commandertvis.plugin.sessionGui
import io.github.commandertvis.rphelper.PLUGIN_NAME
import io.github.commandertvis.rphelper.Permissions
import io.github.commandertvis.rphelper.datastorage.PlayerResourcePackManager
import io.github.commandertvis.rphelper.datastorage.ResourcePack
import io.github.commandertvis.rphelper.messages
import io.github.commandertvis.rphelper.resourcePacks
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.HoverEvent
import org.bukkit.Material
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

private val guiArchetype = sessionGui(6, false, 0) {
    title = messages.gui.title

    (5 on 0).button(ItemStack(Material.ARROW)) {
        name = messages.gui.previousPage

        action {
            gui change@{
                if (paginateResourcePacks(session - 1).isEmpty()) return@change
                session -= 1
                this@action.renderResourcePacks()
            }
        }
    }

    (5 on 8).button(ItemStack(Material.ARROW)) {
        name = messages.gui.nextPage

        action {
            gui change@{
                if (paginateResourcePacks(session + 1).isEmpty()) return@change
                session += 1
                this@action.renderResourcePacks()
            }
        }
    }

    (5 on 3).element(ItemStack(Material.WATER_BUCKET)) {
        name = messages.gui.resetPack
        lore = mutableListOf(messages.gui.lmbToReset, messages.gui.rmbToResetAndLeave)

        onLoaded {
            if (player.hasPermission(Permissions.ADMIN_PERMISSION)) gui {
                5 on 3 update {
                    lore = lore?.toMutableList()?.apply { add(messages.gui.shiftLmbToFlush) }
                }
            }
        }

        onLeftClick { sendCommand("reset") }
        onShiftLeftClick { if (player.hasPermission(Permissions.ADMIN_PERMISSION)) sendCommand("flush") }
        onRightClick { sendCommand("reset kick", false) }
    }

    (5 on 5).button(ItemStack(Material.BARRIER)) {
        name = messages.gui.close
        action { close() }
    }

    rectangle(5 on 1, 5 on 2, ItemStack(Material.GRAY_STAINED_GLASS)) { name = " " }
    (5 on 4).element(ItemStack(Material.GRAY_STAINED_GLASS)) { name = " " }
    rectangle(5 on 6, 5 on 7, ItemStack(Material.GRAY_STAINED_GLASS)) { name = " " }
}

private fun paginateResourcePacks(page: Int): Set<ResourcePack> {
    if (page < 0) return emptySet()
    val firstIndex = 45 * page
    var secondIndex = firstIndex + 44
    val resourcePackList = resourcePacks.toList()
    if (resourcePackList.lastIndex < firstIndex) return emptySet()
    if (resourcePackList.lastIndex < secondIndex) secondIndex = resourcePackList.lastIndex
    return resourcePackList.subList(firstIndex, secondIndex + 1).toSet()
}

private fun GuiView<Int>.sendCommand(command: String, close: Boolean = true) {
    val page = gui.session

    if (close) {
        close()

        player.replyComponents {
            text {
                +messages.gui.returnToGui
                onHover(HoverEvent.Action.SHOW_TEXT) { text { +messages.gui.returnTo } }
                onClick(ClickEvent.Action.RUN_COMMAND, "/$PLUGIN_NAME:rp gui $page")
            }
        }
    }

    player.command("$PLUGIN_NAME:rp $command")
}

private fun GuiView<Int>.renderResourcePacks() {
    gui {
        deleteRectangle(0 on 0, 4 on 8)

        paginateResourcePacks(session).forEachIndexed { index, resourcePack ->
            chestSlot(index).element(ItemStack(resourcePack.guiIcon)) {
                name = "${messages.gui.packPrefix}${resourcePack.name}"

                lore = mutableListOf(
                    if (this@renderResourcePacks.player.hasPermission(resourcePack.permission))
                        messages.command.permissionAvailable
                    else
                        messages.command.permissionNotAvailable,

                    messages.gui.lmbToSelect,
                    messages.gui.rmbToGetInfo
                )

                if (PlayerResourcePackManager[this@renderResourcePacks.player] == resourcePack)
                    lore = lore?.toMutableList()?.apply { add(0, messages.gui.current) }


                onLeftClick { sendCommand("select ${resourcePack.name}") }
                onRightClick { sendCommand("info ${resourcePack.name}") }
            }
        }
    }
}

private fun Player.displayGui(page: Int = 0) = showGui(guiArchetype).run {
    gui { session = page }
    renderResourcePacks()
}

fun ExecutionScope.guiActions() {
    default {
        action(@Contexts
        fun CommandSender.(_: String) {
            if (!hasPermission(Permissions.USER_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return
            }

            if (this !is Player) {
                sendMessage(messages.command.playerOnly)
                return
            }

            displayGui()
        })

        tabAction { _, _, _ -> emptyList() }
    }

    "gui" subcommand {
        action(response = @Contexts(DefaultContext::class)
        fun CommandSender.(_: String, args: List<String>) {
            if (!hasPermission(Permissions.USER_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return
            }

            if (this !is Player) {
                sendMessage(messages.command.playerOnly)
                return
            }

            displayGui(
                page = args.takeIf { it.isNotEmpty() }?.parseArguments(IntContext)?.getOrNull(index = 0) as? Int? ?: 0
            )
        })

        tabAction { _, _, _ -> emptyList() }
    }
}
