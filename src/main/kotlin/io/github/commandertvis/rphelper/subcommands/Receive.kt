package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.dsl.ExecutionScope
import io.github.commandertvis.rphelper.Permissions
import io.github.commandertvis.rphelper.datastorage.PlayerResourcePackManager
import io.github.commandertvis.rphelper.messages
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

fun ExecutionScope.receive() {
    "receive" subcommand {
        action(@Contexts
        fun CommandSender.(_: String) {
            if (!hasPermission(Permissions.USER_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return
            }

            if (this !is Player) {
                sendMessage(messages.command.playerOnly)
                return
            }

            if (this !in PlayerResourcePackManager) {
                sendMessage(messages.command.noResourcePackToReceive)
                return
            }

            PlayerResourcePackManager.send(this)
        })

        tabAction { _, _, _ -> emptyList() }
    }
}
