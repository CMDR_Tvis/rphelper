package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command.contexts.StringContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.dsl.ExecutionScope
import io.github.commandertvis.plugin.placeholder
import io.github.commandertvis.plugin.placeholders
import io.github.commandertvis.plugin.replyComponents
import io.github.commandertvis.rphelper.Permissions
import io.github.commandertvis.rphelper.messages
import io.github.commandertvis.rphelper.resourcePacks
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.HoverEvent
import org.bukkit.command.CommandSender

fun ExecutionScope.info() {
    "info" subcommand {
        action(response = @Contexts(StringContext::class)
        fun CommandSender.(_: String, name: String?) {
            if (!hasPermission(Permissions.USER_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return
            }

            val rp = resourcePacks
                .find { pack -> hasPermission(pack.permission) && pack.name == name }

            rp?.let {
                replyComponents {
                    text {
                        +("${messages.command.resourcePack.header}\n" +
                                "${messages.command.resourcePack.permissionRubric}\n" +
                                "${messages.command.resourcePack.permission}\n" +
                                "${messages.command.resourcePack.urlRubric}\n").placeholders(
                            "name" to it.name,
                            "permission" to it.permission.name,

                            "available" to if (hasPermission(rp.permission))
                                messages.command.permissionAvailable
                            else
                                messages.command.permissionNotAvailable
                        )
                    }

                    text {
                        +"${messages.command.resourcePack.url}\n"
                            .placeholder("url" to it.url.toString())

                        onHover(HoverEvent.Action.SHOW_TEXT) { text { +messages.command.clickToGo } }
                        onClick(ClickEvent.Action.OPEN_URL) { it.url.toString() }
                    }

                    text { +"${messages.command.resourcePack.hashRubric}\n" }

                    text {
                        val hashString = (it.hashString ?: messages.command.nullHash)
                        +"${messages.command.resourcePack.hash}\n".placeholder("hash" to hashString)
                        onHover(HoverEvent.Action.SHOW_TEXT) { text { +messages.command.clickToCopy } }
                        onClick(ClickEvent.Action.SUGGEST_COMMAND) { hashString }
                    }
                }

                return
            }

            sendMessage(messages.command.noSuchResourcePack)
        })

        resourcePackListTabAction()
    }
}
