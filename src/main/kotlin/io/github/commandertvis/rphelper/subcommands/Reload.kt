package io.github.commandertvis.rphelper.subcommands

import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.dsl.ExecutionScope
import io.github.commandertvis.rphelper.Permissions
import io.github.commandertvis.rphelper.messages
import io.github.commandertvis.rphelper.plugin
import org.bukkit.command.CommandSender

fun ExecutionScope.reload() {
    "reload" subcommand {
        action(@Contexts
        fun CommandSender.(_: String) {
            if (!hasPermission(Permissions.ADMIN_PERMISSION)) {
                sendMessage(messages.command.noPermission)
                return@action
            }

            plugin.reloadConfig()
            sendMessage(messages.command.reloaded)
        })

        tabAction { _, _, _ -> emptyList() }
    }
}
