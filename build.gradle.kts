plugins { kotlin("jvm") }
val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val pluginAnnotationsVersion: String by project
val pluginApiVersion: String by project
val rpHelperVersion: String by project
val spigotApiVersion: String by project
description = "A simple to use resource pack management GUI."
group = "io.github.commandertvis"
version = rpHelperVersion

repositories {
    jcenter()
    maven("https://gitlab.com/api/v4/projects/10077943/packages/maven")
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots")
    maven("https://oss.sonatype.org/content/groups/public/")
}

dependencies {
    fun pluginApi(module: String) = "io.github.commandertvis.plugin:$module:$pluginApiVersion"
    compileOnly("org.spigotmc:spigot-api:$spigotApiVersion")
    compileOnly(pluginApi(module = "chat-components"))
    compileOnly(pluginApi(module = "common"))
    compileOnly(pluginApi(module = "command"))
    compileOnly(pluginApi(module = "gui"))
}

tasks {
    compileKotlin.get().kotlinOptions {
        apiVersion = kotlinApiVersion
        jvmTarget = kotlinJvmTarget
        languageVersion = kotlinLanguageVersion
    }

    create<Copy>(name = "copyToRun") { from(jar); into("./run/plugins/") }
    processResources.get().expand("description" to description, "pluginName" to "RPHelper", "version" to version)
}
