# RPHelper [![Pipeline status](https://img.shields.io/gitlab/pipeline/CMDR_Tvis/rphelper?style=flat-square)](https://gitlab.com/CMDR_Tvis/rphelper/commits/master) [![License](https://img.shields.io/badge/license-MIT-44be16?style=flat-square)](LICENSE)

**RPHelper** is a Spigot plugin that allows players to select between multiple resource packs with a pleasant GUI.

## Installing

This plugin is designed for 1.15.2, older versions support is not guaranteed. To install RPHelper, you need to simply
put it into your `/plugins/` folder. RPHelper depends on 
[PluginApi](https://www.spigotmc.org/resources/pluginapi.71657/), you need to have it installed too.

**Note: you shouldn't use vanilla server.properties entries: `resource-pack-sha1` and `resource-pack`.**

## Gallery: 

![](images/gui.png)

![](images/rp-info.png)

![](images/cfg.png)

## Permissions: 

* `rphelper.admin` &mdash; allows executing some privileged commands. Also, guarantees access to all the resource 
packs; 
* `rphelper.user` &mdash; allows interacting with the resourcepack manager;
* `rphelper.resourcepack.<name>` &mdash; allows selecting and download resource pack <name>;

## Commands

* `rp` &mdash; opens the GUI:
    * `reload` &mdash; reloads the settings; 
    * `flush` &mdash; resets the resource pack settings of all the players (`rphelper.admin`);
    * `gui [<page>]` &mdash; opens the GUI, page can be selected optionally (`rphelper.user`); 
    * `info <resource pack name>` &mdash; returns the information (URL and hash) about a certain resource pack 
    (`rphelper.user` + `rphelper.resourcepack.<resource pack name>`); 
    * `list` &mdash; returns the list of all resource packs on the server, available to sender or not 
    (`rphelper.user`); 
    * `receive` &mdash; tries to download selected resource pack (`rphelper.user`);
    * `reset [<kick>]` &mdash; tries to reset your resource pack, if “kick” added as argument, you get kicked from the 
    server (`rphelper.user`);
    * `select <resource pack name>` &mdash; selects a resource pack with a certain name for you and tries to send it 
    (`rphelper.user` + `rphelper.resourcepack.<resource pack name>`). 

## Configuration

RPHelper can be configured using JSON configuration in `/plugins/RPHelper` directory.

Properties: 

* `object messages` &mdash; various messages sent or shown by the plugin: 
    * `object command` &mdash; messages that are sent as a result of command executing:
        * `string clickToGo` &mdash; shown when a player hovers on a link; 
        * `string clickToCopy` &mdash; shown when a player hovers on a copyable message; 
        * `string description` &mdash; the description of the root command;
        * `string flushed` &mdash; sent when all the resource packs data gets flushed;
        * `string noPermission` &mdash; sent when a player has no access to a certain subcommand;
        * `string noSuchResourcePack` &mdash; sent when a player tries to access an unavailable resource pack;
        * `string nullHash` &mdash; the placeholder of resource pack's hash if it is undefined;
        * `string permissionAvailable` &mdash; the flag of available permission; 
        * `string permissionNotAvailable` &mdash; the flag of unavailable permission; 
        * `string playerOnly` &mdash; sent when a player-only command is called not by a player; 
        * `string reset` &mdash; sent when a player resets his resource-pack; 
        * `string resetKick` &mdash; sent when a player resets his resource-pack and gets kicked by the plugin;
        * `object resourcePack` &mdash; the resource pack info message: 
            * `string header` &mdash; the header of resource pack description. There is an available placeholder: 
            `%name%` &mdash; the name of the resource pack; 
            * `string permissionRubric` &mdash; the caption of permission value,
            * `string permission` &mdash; the permission value. Available placeholders: `%permission%` &mdash; the
            permission value, `%available%` &mdash; the state if this permission available to sender (takes either 
            `permissionAvailable` or `permissionNotAvailable` value). 
    * `object permission` &mdash; messages that describe the permissions of the plugin:
        * `string admin` &mdash; describes `rphelper.admin` permission,
        * `string user` &mdash; describes `rphelper.user` permission,
        * `string resourcePack` &mdash; describes `rphelper.resourcepack.*` permission;
    * `object gui` &mdash; phrases that are displayed in the GUI:
        * `string close` &mdash; the button for closing the GUI, 
        * `string current` &mdash; the label of the current resource pack,
        * `string lmbToReset` &mdash; the lore of the button to reset the resource pack,
        * `string nextPage` &mdash; the label of the button to go to the next page,
        * `string rmbToResetAndLeave` &mdash; the lore of the button to reset the resource pack (leaving action 
        description),
        * `string lmbToSelect` &mdash; the label of resource pack button describing the selection of resource pack,
        * `string packPrefix` &mdash; the prefix of resource pack name in the GUI list,
        * `string previousPage` &mdash; the label of the button to go to the previous page,
        * `string resetPack` &mdash; the label of the resource pack resetting button,
        * `string returnTo` &mdash; the hover label of chat message in the chat that allows returning to the GUI,
        * `string returnToGui` &mdash; the chat message in the chat that allows returning to the GUI,
        * `string rmbToGetInfo` &mdash; the label of the button describing the getting info message of resource pack,
        * `string shiftLmbToFlush` &mdash; admin-only Shift+LMB action of the resetting button,
        * `string title` &mdash; the title of GUI; 
    * `string tryAgain` &mdash; the try again hover message; 
    * `string loadFailure` &mdash; the resource pack load message; 
    * `string clickToTryAgain` &mdash; shown when a resource pack failed to download; 
    * `string success` &mdash; sent when a player successfully receives a resource pack of the plugin;
* `array of objects resourcePacks` &mdash; stores available resource packs: 
    * `string name` &mdash; the name of the resource pack. Must be unique;
    * `string url` &mdash; the URL to download this resource pack; 
    * `string hashString` &mdash; the hex encoded SHA1 checksum of this resource pack;
    * `string guiIcon` &mdash; the [material](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html) that 
    represents this resource pack in the GUI. 


## Licensing

This project is licensed under the [MIT license](LICENSE).
